from reactor.helpers import *

################################################################################

@Reactor.social.register_auth('pinterest', 'PinterestOAuth2',
    register_url='https://developers.pinterest.com/docs/api/authentication/',
    overview_url='',
mode='oauth2')
def detect_pinterest(cfg):
    yield 'KEY', cfg.get('access_key', '')
    yield 'SECRET', cfg.get('access_pki', '')

################################################################################


