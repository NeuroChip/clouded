from reactor.helpers import *

################################################################################

@Reactor.social.register_auth('twilio', 'TwilioOAuth',
    register_url='https://www.twilio.com/user/account/connect/apps',
    overview_url='',
mode='oauth2')
def detect_twilio(cfg):
    yield 'KEY', cfg.get('key', 'ACd11a5e5d4e783959171ce5022bb80346')
    yield 'SECRET', cfg.get('pki', '1b338e6c8a6bde77ef5a58a77dedb865')

#*******************************************************************************

@Reactor.social.register_auth('slack', 'SlackOAuth2',
    register_url='',
    overview_url='',
mode='oauth2')
def detect_slack(cfg):
    yield 'KEY', cfg.get('key', '4932747637.73508214134')
    yield 'SECRET', cfg.get('pki', '9f0079152e56cf28338b7e8cda3fec79')

    if False:
        #yield 'SCOPE', ['identity.basic']
        yield 'SCOPE', [
            '%s:%s' % (group,item)
            for group,listing in [
                ('chat:write',    ['bot','user']),
                ('files',         ['read','write:user']),

                ('groups',        ['read','write','history']),
                ('channels',      ['read','write','history']),
                ('im',            ['read','write','history']),
                ('mpim',          ['read','write','history']),

                ('search',        ['read']),
                ('team',          ['read']),

                ('dnd',           ['read','write']),
                ('pins',          ['read','write']),
                ('stars',         ['read','write']),
                ('reactions',     ['read','write']),
                ('reminders',     ['read','write']),

                ('usergroups',    ['read','write']),
                ('users',         ['read','write']),
                ('users.profile', ['read','write']),
            ]
            for item in listing
        ]

#*******************************************************************************

@Reactor.social.register_auth('line', 'LineOAuth2',
    register_url='',
    overview_url='',
mode='oauth2')
def detect_line(cfg):
    yield 'KEY', cfg.get('key', 'c65e3f78706e63f4efe83a12a9329ad5')
    yield 'SECRET', cfg.get('pki', '9623e3ab782c0a615ecbf838ccf369cfafc7f5f326ca40df764277082dfc1c01')

#*******************************************************************************

@Reactor.social.register_auth('weixin', 'WeixinOAuth2')
def detect_weixin(cfg):
    yield 'KEY', cfg.get('key', 'xxxxxx')
    yield 'SECRET', cfg.get('pki', 'xxxxxx')

