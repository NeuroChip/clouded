from reactor.helpers import *

################################################################################

@Reactor.social.register_auth('withings', 'WithingsOAuth')
def detect_withings(cfg):
    yield 'KEY', cfg.get('key', 'xxxxxx')
    yield 'SECRET', cfg.get('pki', 'xxxxxx')

