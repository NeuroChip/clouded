from reactor.shortcuts import *

################################################################################

@Reactor.social.register_api('linkedin')
class LinkedIn(Reactor.social.API):
    def prepare(self):
        self.register('person',  LinkedInUser)
        self.register('company', LinkedInCompany)

    def request(self, method, url, params={}, data=None, headers={}):
        url = 'https://api.linkedin.com/%s/%s' % (self.VERSION, url)

        params['format']       = 'json'
        params['access_token'] = self.creds['access_token']

        return self.http_req(method, url, params, data, headers)

    VERSION = 'v1'

    @property
    def profile(self):
        resp = self.http_get('people/%s' % '~')

        return resp

#*******************************************************************************

@Reactor.social.register_resource('linkedin','user')
class LinkedInUser(Reactor.social.Resource):
    @classmethod
    def listing(cls, api, **query):
        resp = api.http_get('people/~')

        for person in resp['data']:
            #Reactor.wamp.publish(u"reactor.perso.social.knows", 'instagram', profile, person, RATINGs[depth+1])

            yield person

    @classmethod
    def narrow(cls, api, **uid):
        pass

    NARROW = ['id']

    def prepare(self):
        pass

#*******************************************************************************

@Reactor.social.register_resource('linkedin','company')
class LinkedInCompany(Reactor.social.Resource):
    @classmethod
    def listing(cls, api, **query):
        resp = api.http_get('companies',params={'is-company-admin':'true'})

        for company in resp['data']:
            #Reactor.wamp.publish(u"reactor.perso.social.knows", 'instagram', profile, person, RATINGs[depth+1])

            yield company

    @classmethod
    def narrow(cls, api, **uid):
        pass

    NARROW = ['id']

    def prepare(self):
        pass

